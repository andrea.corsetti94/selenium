package examples;

import org.openqa.selenium.By;
import org.openqa.selenium.InvalidSelectorException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class Wait {

    public static void threadSleep(long ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            System.out.println("Interrupted Exception while sleeping for " + ms + " milliseconds");
            System.out.println(e.getMessage());
        }
    }

    public static WebElement waitUntilItemIsPresentAndReturnIt(By by, WebDriver driver, long msToWait){

        try {
            WebDriverWait wait = new WebDriverWait(driver, msToWait);
            return wait.until(ExpectedConditions.presenceOfElementLocated(by));
        }
        catch (InvalidSelectorException ex){
            System.out.println("InvalidSelectorException. By: " + by.toString());
            System.out.println(ex.getMessage());
            System.out.println("Returning null element");
            return null;
        }
    }

    public static void implicitWaitPage(WebDriver driver, long unit, TimeUnit timeUnit){
        driver.manage().timeouts().implicitlyWait(unit, timeUnit);
    }

    public static WebElement explicitWaitPage(WebDriver driver, long seconds, By locator){
        WebDriverWait webDriverWait = new WebDriverWait(driver, seconds);

        return webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
}
