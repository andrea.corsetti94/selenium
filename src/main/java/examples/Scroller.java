package examples;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class Scroller {

    public static Actions scroll(WebDriver driver, WebElement element){
        if ( null == element ){
            System.out.println("Can't scroll to element: it's null. Returning null Actions");
            return null;
        }
        Actions actions = new Actions(driver);
        return actions.moveToElement(element);
    }
}
