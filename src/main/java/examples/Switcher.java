package examples;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;

public class Switcher {

    public static void switchToWindow(String windowName, WebDriver driver){

        //driver.getWindowHandles() returns the handles for every tab in the browser
        //driver.getWindowHandle() returns the handle for the opened tab in the browser

        for( String handle: driver.getWindowHandles() ){ //tabs opened
            if ( handle.equals(windowName) ){
                driver.switchTo().window(handle);
                return;
            }
        }
        System.out.println("Warn: No window handle with name windowName to switch tab for");
    }


    public static void switchToFrame(String framename) {

    }


    public static void switchToAlert(WebDriver driver) {
        //Pre-requisite: an alert is opened:
        Alert alert = driver.switchTo().alert();
        // alert.accept(); closes it
        // alert.dismiss(); closes it too
    }
}
