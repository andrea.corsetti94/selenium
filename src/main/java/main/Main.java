package main;

import config.DriverManager;
import examples.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String args[]){

        WebDriver driver = DriverManager.getWebDriver();

        driver.get("https://www.gazzetta.it/");

        WebElement element = FindElements.findByXPath(driver,
                "///html/body/footer/div[1]/div/div[2]/div[2]/div[2]/div[1]/ul/li[1]/a");

        Scroller.scroll(driver, element);

        Mouse.click(element);

        driver.quit();

    }

    //https://www.testingexcellence.com/selenium-tutorial/
}

