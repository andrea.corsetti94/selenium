package config;

import org.openqa.selenium.WebDriver;

public class DriverManager {

    private static WebDriver webDriver;

    private static WebDriver createWebDriver(){
        String driverCreatorClassname = Config.getDriverCreatorInstance();
        DriverFactory driverFactory = null;
        try{
            driverFactory = (DriverFactory) Class.forName(driverCreatorClassname).newInstance();
        }

        catch (ClassCastException ex1){
            System.out.println("Instance of : " + driverCreatorClassname + " is not a DriverFactory.\n" + ex1.getMessage());
            System.exit(-1);
        } catch (ClassNotFoundException ex4) {
            System.out.println("Class: " + driverCreatorClassname + " not found at startup.\n"+ ex4.getMessage());
            System.exit(-1);
        } catch ( Exception ex3 ){
            System.out.println("Exception in reflective method that creates appropriate web driver\n" + ex3.getMessage());
            System.exit(-1);
        }
        return driverFactory.createWebDriver();
    }

    public static WebDriver getWebDriver(){
        if ( null == webDriver ){
            webDriver = createWebDriver();
        }
        return webDriver;
    }
}
