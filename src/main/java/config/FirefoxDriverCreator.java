package config;

import org.openqa.selenium.WebDriver;

class FirefoxDriverCreator extends DriverFactory {

    private static final String DRIVER_SYSTEM_PROPERTY = "webdriver.firefox.driver";

    @Override
    public WebDriver createWebDriver() {
        throw new UnsupportedOperationException("No IE Driver supported at the moment");
    }
}
