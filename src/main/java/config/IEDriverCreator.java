package config;

import org.openqa.selenium.WebDriver;

class IEDriverCreator extends DriverFactory {

    private static final String DRIVER_SYSTEM_PROPERTY = "webdriver.ie.driver";

    @Override
    public WebDriver createWebDriver() {
        throw new UnsupportedOperationException("No IE Driver supported at the moment");
    }

}
