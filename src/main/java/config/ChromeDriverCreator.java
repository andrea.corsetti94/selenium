package config;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

class ChromeDriverCreator extends DriverFactory {

    private static final String DRIVER_SYSTEM_PROPERTY = "webdriver.chrome.driver";

    @Override
    public WebDriver createWebDriver() {
        System.setProperty(DRIVER_SYSTEM_PROPERTY, Config.getLocalChromeDriverPath());
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        return new ChromeDriver(options);
    }

}
