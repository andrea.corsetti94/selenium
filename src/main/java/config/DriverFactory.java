package config;

import org.openqa.selenium.WebDriver;

abstract class DriverFactory {
    abstract WebDriver createWebDriver();
}
