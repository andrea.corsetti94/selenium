package config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Properties;

public class Config {

    private static Properties properties;
    private static final String APPLICATION_PROPERTY_FILENAME = "/application.properties";

    static {
        properties = new Properties();
        String resourcesPath = "";
        try {
            resourcesPath = new File("src/main/resources/").getAbsolutePath();
            properties.load(new FileInputStream(resourcesPath + APPLICATION_PROPERTY_FILENAME));

        } catch (IOException e) {
            System.out.println("IOException raised while loading file: " + resourcesPath + APPLICATION_PROPERTY_FILENAME);
            System.out.println(e.getMessage());
            System.exit(-1);
        }
    }

    public static String getDriverCreatorInstance(){
        String driverPropertyKey = "driverCreatorInstance";
        return properties.getProperty(driverPropertyKey);
    }

    public static String getLocalChromeDriverPath() {
        String chromeDriverPathKey = "chromeDriverPath";
        return properties.getProperty(chromeDriverPathKey);
    }


}
